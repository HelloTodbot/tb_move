#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32

def servo_fine():
    m1Pub = rospy.Publisher('motorL_fine', Int32, queue_size=10)
    m2Pub = rospy.Publisher('motorR_fine', Int32, queue_size=10)
    rospy.init_node('fineMotorControl', anonymous=True)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        m1 = input("Motor 1 (R) (0-400): ")
        m2 = input("Motor 2 (L) (0-400): ")
        m1Pub.publish(m1)
        m2Pub.publish(m2)
	rospy.loginfo(m1)
	rospy.loginfo(m2)
        rate.sleep()

if __name__ == '__main__':
    try:
        servo_fine()
    except rospy.ROSInterruptException:
        pass
